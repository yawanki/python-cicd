#this code prints items in a list
fruits=["banana","oranges","apples","peas"]
for x in fruits:
    print(x)

print("\n")
#this code prints items in a dictionary
cars={
  "make": "Audi",
  "model": "A5",
  "year": 2017,
  "price": "£5000"
}
for k in cars:
      print(k)
